﻿using System;


namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 module1 = new Module1();

            int[] array = module1.SwapItems(10, 20);
            Console.WriteLine("SwapItems:\t{0}\t{1}", array[0],array[1]);
            
            array = new int[5] { 1, 5, 2, 4, 3 };
            Console.WriteLine("GetMinimumValue:\t{0}",module1.GetMinimumValue(array));

            Console.ReadLine();
        }


        public int[] SwapItems(int a, int b)
        {
            return new int[2] { b, a };
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];
            for (int i = 1; i < input.Length; i++)
            {
                if (input[i]<min)
                {
                    min = input[i];
                }
            }
            return min;
        }
    }
}
